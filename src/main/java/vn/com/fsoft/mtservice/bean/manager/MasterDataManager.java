package vn.com.fsoft.mtservice.bean.manager;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import vn.com.fsoft.mtservice.object.entities.MasterDataParameterEntity;
import vn.com.fsoft.mtservice.object.entities.MasterDataTableEntity;
import vn.com.fsoft.mtservice.util.NumberUtils;

import java.util.*;

/**
 * 
 * @author hungxoan
 *
 */
@Component("masterDataManager")
public class MasterDataManager {
	
	private Map<String, MasterDataTableEntity> entityMap = new HashMap<String, MasterDataTableEntity>();
    private Map<Integer, MasterDataTableEntity> entities = new HashMap<Integer, MasterDataTableEntity>();
    private Map<String, Set<MasterDataTableEntity>> masterDataByCategory = new
            HashMap<String, Set<MasterDataTableEntity>>();
    private Set<String> masterCategories = new HashSet<String>();
    private SessionFactory sessionFactory;
    private HibernateTemplate hibernateTemplate;

    public MasterDataManager(@Autowired SessionFactory sessionFactory) {

        hibernateTemplate = new HibernateTemplate(sessionFactory);
        getMasterDataTable();
        getMasterParameters();
    }

    public synchronized MasterDataTableEntity get(Integer id) {

        if(entities.containsKey(id)) {
            return entities.get(id);
        }

        MasterDataTableEntity entity = getMasterDataRecord(id);
        if(entity != null) {
            entities.put(id, entity);
            addMasterDataByCategory(entity);
            return entity;
        }

        return null;
    }

    public Boolean isMasterParamContained(String masterCategory) {
        if(masterCategories.contains(masterCategory)) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    public synchronized Boolean addEntity(MasterDataTableEntity entity) {

        if(entity != null && !NumberUtils.isEmpty(entity.getId())) {

            entities.put(entity.getId(), entity);
            addMasterDataByCategory(entity);
            entityMap.put(entity.getKey(), entity);
            
        }

        return Boolean.FALSE;
    }

    public synchronized Boolean updateEntity(MasterDataTableEntity entity) {

        if(entity != null && !NumberUtils.isEmpty(entity.getId())) {

            entities.put(entity.getId(), entity);
            addMasterDataByCategory(entity);
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    public synchronized Boolean deleteEntity(Integer id) {

        //entities.remove(id);
        MasterDataTableEntity entity = entities.remove(id);
        if (masterDataByCategory.containsKey(entity.getMasterCategory())) {
            masterDataByCategory.get(entity.getMasterCategory()).remove(entity);
        }
        return Boolean.TRUE;
    }

    private void getMasterDataTable() {

        String hql = "select m from MasterDataTableEntity m where m.lock = :lock";
        List<MasterDataTableEntity> entityList = null;
        try {
            entityList = (List<MasterDataTableEntity>) hibernateTemplate.findByNamedParam(hql, "lock",
                    Boolean.TRUE);
        } catch (DataAccessException ex) {
            // log ex
        }

        if(!CollectionUtils.isEmpty(entityList)) {

            entities = new HashMap<Integer, MasterDataTableEntity>();
            for(MasterDataTableEntity entity : entityList) {

                entities.put(entity.getId(), entity);
                addMasterDataByCategory(entity);
            }
        }
    }

    private MasterDataTableEntity getMasterDataRecord(Integer id) {
        String hql = "select m from MasterDataTableEntity m where m.lock = :lock and m.id = :id";
        List<MasterDataTableEntity> entityList = null;
        try {
            entityList = (List<MasterDataTableEntity>) hibernateTemplate.findByNamedParam(hql,
                    new String[] {"lock", "id"}, new Object[] {Boolean.TRUE, id});
        } catch (DataAccessException ex) {
            // log ex
            return null;
        } catch (ObjectNotFoundException ex) {
            // log ex
            return null;
        }

        if(CollectionUtils.isEmpty(entityList)) {
            return null;
        }

        return entityList.get(0);
    }

    private void getMasterParameters() {

        String hql = "select p from MasterDataParameterEntity p where p.used = :used " +
                "order by p.masterCategory ";
        List<MasterDataParameterEntity> entityList = null;

        try {
            entityList = (List<MasterDataParameterEntity>) hibernateTemplate
                    .findByNamedParam(hql,
                    "used", Boolean.TRUE);
        } catch (DataAccessException ex) {
            // log ex
            return;
        }

        if(!CollectionUtils.isEmpty(entityList)) {
            for(MasterDataParameterEntity entity : entityList) {
                if(!masterCategories.contains(entity.getMasterCategory())) {
                    masterCategories.add(entity.getMasterCategory());
                }
            }
        }
    }

    public synchronized Set<MasterDataTableEntity> getByMasterCategory(String masterCategory) {

        if (masterDataByCategory.containsKey(masterCategory)) {
            return masterDataByCategory.get(masterCategory);
        }

        String hql = "Select m from MasterDataTableEntity m Where m.masterCategory = :name " +
                "And m.lock = :lock";
        try {

            Set<MasterDataTableEntity> set = new HashSet<>();
            List<MasterDataTableEntity> list = (List<MasterDataTableEntity>) hibernateTemplate
                    .findByNamedParam(hql, new String[]{"name", "lock"},
                            new Object[]{masterCategory, Boolean.TRUE});
            set.addAll(list);
            if (list != null && !list.isEmpty()) {
                masterDataByCategory.put(masterCategory, set);
            }

            return set;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void addMasterDataByCategory(MasterDataTableEntity entity){

        Set<MasterDataTableEntity> set = null;

        if (masterDataByCategory.containsKey(entity.getMasterCategory())) {

            set = masterDataByCategory.get(entity.getMasterCategory());
        } else {

            set = new HashSet<>();
            masterDataByCategory.put(entity.getMasterCategory(), set);
        }
        set.remove(entity);
        set.add(entity);
    }
    
    public synchronized MasterDataTableEntity get(String key){

        if(entityMap.containsKey(key)){
            return entityMap.get(key);
        }
        MasterDataTableEntity entity = null;
        try{

            entity = getByKey(key).iterator().next();
        } catch (NoSuchElementException ex) {

            return null;
        }
        if (entity != null) {
            entities.put(entity.getId(), entity);
            entityMap.put(key, entity);
            addMasterDataByCategory(entity);
            return entity;
        }

        return null;
    }
    public synchronized Set<MasterDataTableEntity> getByKey(String key) {

        if (masterDataByCategory.containsKey(key)) {
            return masterDataByCategory.get(key);
        }

        String hql = "Select m from MasterDataTableEntity m Where m.key = :name " +
                "And m.lock = :lock";
        try {

            Set<MasterDataTableEntity> set = new HashSet<>();
            List<MasterDataTableEntity> list = (List<MasterDataTableEntity>) hibernateTemplate
                    .findByNamedParam(hql, new String[]{"name", "lock"},
                            new Object[]{key, Boolean.TRUE});
            set.addAll(list);
            if (list != null && !list.isEmpty()) {
                masterDataByCategory.put(key, set);
            }


            return set;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
