package vn.com.fsoft.mtservice.object.models;

import vn.com.fsoft.mtservice.object.base.ResultTransformer;

/**
 * 
 * @author hungxoan
 *
 */
public class RecordExistModel extends ResultTransformer {

    private Boolean result;

    public RecordExistModel() {
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }
}
