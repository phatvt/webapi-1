package vn.com.fsoft.mtservice.object.base;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import vn.com.fsoft.mtservice.constants.CommonConstants;
import vn.com.fsoft.mtservice.object.tokenizing.Tokenizor;

/**
 * 
 * @author hungxoan
 *
 */
public class APIPaginationHelper {

    private static Map<Integer, Map<Integer, List<String>>> paginationTokens = new HashMap<Integer, Map<Integer, List<String>>>();

    public static void generateToken(Integer projectId, List<Integer> userIdList, Integer pageCount) {

        if(null==projectId || projectId<0 || CollectionUtils.isEmpty(userIdList) || null==pageCount||pageCount<0)
            return;

        Map<Integer, List<String>> tokenMapByUser = new HashMap<Integer, List<String>>();
        for(Integer userId : userIdList) {

            String token = CommonConstants.EMPTY;
            if(pageCount > 1) {

                List<String> tokenList = new ArrayList<String>();
                for(int i = 0 ; i < pageCount; i++) {

                    token = Tokenizor.generateToken();
                    tokenList.add(token);
                }

                if(tokenList.size() == pageCount) {
                    tokenMapByUser.put(userId, tokenList);
                    tokenList = new ArrayList<String>();
                }
            } else {
                token = Tokenizor.generateToken();
                tokenMapByUser.put(userId, Arrays.asList(token));
            }
        }

        paginationTokens.put(projectId, tokenMapByUser);
    }

    public static Boolean consumeToken(Integer projectId, Integer userId,int step, String token) {

        if(token==null || step <0)
            return Boolean.FALSE;
        if(projectId==null || userId == null)
            return Boolean.FALSE;
        if(paginationTokens.containsKey(projectId)){
            if(paginationTokens.get(projectId).containsKey(userId)){
                if(token.equals(paginationTokens.get(projectId).get(userId).get(step-1))){
                    paginationTokens.get(projectId).get(userId).set(step-1, Tokenizor.generateToken());
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    public static String getTokenByProjectAndUser(Integer projectId, Integer userId, int step){
        if(paginationTokens.containsKey(projectId)){
            if(paginationTokens.get(projectId).containsKey(userId)){
                return paginationTokens.get(projectId).get(userId).get(step-1);
            }
        }
        return null;
    }

}