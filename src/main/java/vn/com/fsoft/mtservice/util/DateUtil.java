////////////////////////////////////////////////////////////
// DateUtil.java
// Version: 1.0
// Author: (FPT) PhatVT1
// Create date: 2018/05/26
// Update date: -
////////////////////////////////////////////////////////////
package vn.com.fsoft.mtservice.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmssS");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
