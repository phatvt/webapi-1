package vn.com.fsoft.mtservice.controller.command;

import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import vn.com.fsoft.mtservice.constants.CommonConstants;
import vn.com.fsoft.mtservice.controller.BaseControllerTemplate;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.constant.enumeration.FunctionEnum;
import vn.com.fsoft.mtservice.object.form.RestForm;
import vn.com.fsoft.mtservice.util.NumberUtils;

/**
 * 
 * @author hungxoan
 *
 * @param <F>
 */
public abstract class CommandControllerTemplate<F extends RestForm> extends BaseControllerTemplate {

    public final ResponseEntity<String> save(IncomingRequestContext context,
                                             FunctionEnum functionEnum,
                                             RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        // check token and role

        String body = CommonConstants.EMPTY;
        body = saveDefinition(context, requestEntity);

        return new ResponseEntity<String>(body, null, HttpStatus.OK);
    }

    public final ResponseEntity<String> update(IncomingRequestContext context,
                                               FunctionEnum functionEnum,
                                               RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        // check token and role

        String body = CommonConstants.EMPTY;
        body = updateDefinition(context, requestEntity);

        return new ResponseEntity<String>(body, null, HttpStatus.OK);

    }

    public final ResponseEntity<String> delete(IncomingRequestContext context, Integer id,
                                               FunctionEnum functionEnum,
                                               RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        if(NumberUtils.isEmpty(id)) {
            return reject(new ValidationStatus("500", "Request data is invalid."));
        }

        // check token and role

        String body = CommonConstants.EMPTY;
        body = deleteDefinition(context, id, requestEntity);

        return new ResponseEntity<String>(body, HttpStatus.OK);
    }

    public final ResponseEntity<String> bulk(IncomingRequestContext context,
                                             FunctionEnum functionEnum,
                                             RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        // check token and role

        String body = CommonConstants.EMPTY;
        body = bulkDefinition(context, requestEntity);

        return new ResponseEntity<String>(body, null, HttpStatus.OK);
    }

    protected abstract String saveDefinition(IncomingRequestContext context, RequestEntity<F> requestEntity);
    protected abstract String updateDefinition(IncomingRequestContext context, RequestEntity<F> requestEntity);
    protected abstract String deleteDefinition(IncomingRequestContext context, Integer id,
                                     RequestEntity<F> requestEntity);
    protected abstract String bulkDefinition(IncomingRequestContext context, RequestEntity<F> requestEntity);
}
